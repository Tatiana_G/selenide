Feature: Send a message to support

  Scenario: Send a message
    Given Main page is open
    Then Click Contact us button
    And Choose Subject
    And Input Email and Order reference
    And Input Messages
    And Click Send
    Then alert success is visible