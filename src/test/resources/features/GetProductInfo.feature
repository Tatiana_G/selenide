Feature: Get more details about product item
  Choose product item, open pictures, choose size and add to wishlist

  Background:
    When Click "Sign In" button
    And Input login and password
    And Click "Sign in" submit button
    Then Redirect to main page

  Scenario:
    Given Main page is open
    When Choose product item
    Then Check pictures
    Then Add Quantity and Size
    Then Add to wishlist
