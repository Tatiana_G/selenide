package steps;

import cases.ContactUs;
import cases.SignIn;
import config.UserConfig;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class ContactUsStepdefs {

    ContactUs contactUs = new ContactUs();
    SignIn signIn = new SignIn();

    @Then("Click Contact us button")
    public void clickContactUsButton(){
        contactUs.contactUsButtonClick();
    }

    @And("Choose Subject")
    public void chooseSubject(){
        contactUs.chooseSubject();
    }

    @And("Input Email and Order reference")
    public void inputEmail(){
        signIn.inputLogin(UserConfig.USER_LOGIN);
        contactUs.inputOrderReference(ContactUs.order_reference);
    }

    @And("Input Messages")
    public void inputMessage(){
        contactUs.inputMessage(ContactUs.supportMessage);
    }

    @And("Click Send")
    public void clickSendButton(){
        contactUs.clickSendButton();
    }

    @Then("alert success is visible")
    public void messageSentContent(){
        contactUs.messageSentContent();
    }
}
