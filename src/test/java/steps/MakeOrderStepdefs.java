package steps;


import cases.MakeOrder;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class MakeOrderStepdefs {
    MakeOrder makeOrder = new MakeOrder();

        @Given("Main page is open")
        public void mainPageIsOpen() {
            makeOrder.mainPageIsOpen();
        }

        @When("Choose menu")
        public void chooseMenu() {
            makeOrder.chooseMenu();
            makeOrder.chooseSubmenu();
        }

        @Then("Setup size checkbox")
        public void setupSizeCheckbox() {
            makeOrder.setupSizeCheckbox();
        }

        @Then("Choose dress and add to cart")
        public void chooseDress() {
            makeOrder.chooseDress();
            makeOrder.redirectToCart();
        }

        @Then("Choose address and shipping")
        public void delivery(){
            makeOrder.choseAddressPTCButton();
        }

        @Then("Payment")
        public void payment(){
            makeOrder.setPayByBankWire();
        }

        @Then("Confirm order")
        public void confirmOrder(){
            makeOrder.confirmation();
        }
}
