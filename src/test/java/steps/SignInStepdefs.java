package steps;

import cases.SignIn;
import config.UserConfig;
import io.cucumber.java.en.Then;

public class SignInStepdefs {

    SignIn signIn = new SignIn();

    @Then("Click {string} button")
    public void clickButton(String arg0){
        signIn.clickButton(arg0);
    }

    @Then("Input login and password")
    public void inputLogin() {
        signIn.inputLogin(UserConfig.USER_LOGIN);
        signIn.inputPassword(UserConfig.USER_PASSWORD);
    }

    @Then("Click {string} submit button")
    public void clickSubmitLoginButton(String arg0) {
        signIn.clickSubmitLoginButton(arg0);
    }

    @Then("Redirect to main page")
    public void redirectToMainPage(){
        signIn.yourLogoClick();
    }

}
