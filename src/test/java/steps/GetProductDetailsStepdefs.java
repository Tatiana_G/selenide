package steps;

import cases.GetProductDetails;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GetProductDetailsStepdefs {

    GetProductDetails getProductDetails = new GetProductDetails();

    @When("Choose product item")
    public void chooseProduct(){
        getProductDetails.chooseProduct();
    }

    @Then("Check pictures")
    public void checkPictures() {
        getProductDetails.getImage();
    }

    @Then("Add Quantity and Size")
    public void addQuantityAndSize() {
        getProductDetails.getMSize();
        getProductDetails.getQuantity();
    }

    @Then("Add to wishlist")
    public void addToWishlist() {
        getProductDetails.addToWishList();
    }
}
