package locators;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class BasicElements {

    public SelenideElement singInButton = $(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a"));
    public SelenideElement loginInput = $(By.xpath("//*[@id=\"email\"]"));
    public SelenideElement passwordInput = $(By.xpath("//*[@id=\"passwd\"]"));
    public SelenideElement yourLogo = $(By.xpath("//*[@id=\"header_logo\"]/a/img"));
    public SelenideElement dressButton = $(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/a"));
    public SelenideElement casualDressesElement = $(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/ul/li[1]/a"));
    public SelenideElement firstDress = $(By.xpath("//*[@id=\"center_column\"]/ul/li/div/div[1]/div"));
    public SelenideElement addToCartButton = $(By.xpath("//*[@id=\"center_column\"]/ul/li/div/div[2]/div[2]/a[1]"));
    public SelenideElement successfullyAddedString = $(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[1]/h2/i"));
    public SelenideElement proceedToCheckoutButton = $(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a"));
    public SelenideElement cartPTCButton = $(By.xpath("//*[@id=\"center_column\"]/p[2]/a[1]/span"));
    public SelenideElement addressProceedToCheckoutButton = $(By.xpath("//*[@id=\"center_column\"]/form/p/button/span"));
    public SelenideElement shippingCheckbox = $(By.xpath("//*[@id=\"cgv\"]"));
    public SelenideElement shippingPTCButton = $(By.xpath("//*[@id=\"form\"]/p/button"));
    public SelenideElement payByBankWire = $(By.xpath("//*[@id=\"HOOK_PAYMENT\"]/div[1]/div/p/a"));
    public SelenideElement confirmButton = $(By.xpath("//*[@id=\"cart_navigation\"]/button"));
    public SelenideElement orderConfirmation = $(By.xpath("//*[@id=\"center_column\"]/h1"));
    public SelenideElement contactUsButton = $(By.xpath("//*[@id=\"contact-link\"]/a"));
    public SelenideElement customerServiceContent = $(By.xpath("//*[@id=\"center_column\"]/h1"));
    public SelenideElement subjectDropdown = $(By.xpath("//*[@id=\"id_contact\"]"));
    public SelenideElement orderReferenceInput = $(By.xpath("//*[@id=\"id_order\"]"));
    public SelenideElement contactUsMessage = $(By.xpath("//*[@id=\"message\"]"));
    public SelenideElement sendButton= $(By.xpath("//*[@id=\"submitMessage\"]/span"));
    public SelenideElement messageSent = $(By.xpath("//*[@id=\"center_column\"]/p"));
    public SelenideElement blackShirt = $(By.xpath("//*[@id=\"homefeatured\"]/li[2]/div/div[1]/div/a[1]"));
    public SelenideElement blackShirtMoreBtn = $(By.xpath("//*[@id=\"homefeatured\"]/li[2]/div/div[2]/div[2]/a[2]/span"));
    public SelenideElement imagineBlock = $(By.xpath("//*[@id=\"image-block\"]"));
    public SelenideElement imageFancyBox = $(By.xpath("//*[@id=\"product\"]/div[2]/div"));
    public SelenideElement imageFancyBoxClose = $(By.xpath("//*[@id=\"product\"]/div[2]/div/a"));
    public SelenideElement itemColorWhite = $(By.xpath("//*[@id=\"color_8\"]"));
    public SelenideElement itemQuantityPlusOneBtn = $(By.xpath("//*[@id=\"quantity_wanted_p\"]/a[2]"));
    public SelenideElement addToWishListBtn = $(By.xpath("//*[@id=\"wishlist_button\"]"));
    public SelenideElement successfullyAddedWishList = $(By.xpath("//*[@id=\"product\"]/div[2]/div/div/div/div"));
    public SelenideElement successfullyAddedWishListClose = $(By.xpath("//*[@id=\"product\"]/div[2]/div/div/a"));

}
