package cases;

import com.codeborne.selenide.Condition;
import locators.BasicElements;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ContactUs {

    BasicElements basicElements = new BasicElements();

    /* private SelenideElement contactUsButton = $(By.xpath("//*[@id=\"contact-link\"]/a"));
    private SelenideElement customerServiceContent = $(By.xpath("//*[@id=\"center_column\"]/h1"));
    private SelenideElement subjectDropdown = $(By.xpath("//*[@id=\"id_contact\"]"));
    private SelenideElement orderReferenceInput = $(By.xpath("//*[@id=\"id_order\"]"));
    private SelenideElement contactUsMessage = $(By.xpath("//*[@id=\"message\"]"));
    private SelenideElement sendButton= $(By.xpath("//*[@id=\"submitMessage\"]/span"));
    private SelenideElement messageSent = $(By.xpath("//*[@id=\"center_column\"]/p"));

     */


    public void contactUsButtonClick(){
        basicElements.contactUsButton.click();
        basicElements.customerServiceContent.shouldBe(Condition.visible);
    }

    public void chooseSubject(){
        basicElements.subjectDropdown.click();
        $(By.id("id_contact")).selectOptionByValue("1");
    }

    public static final String order_reference = "aaaaa";
    public static final String supportMessage = "1111111";

    public void inputOrderReference(String text){
        basicElements.orderReferenceInput.val(text);
    }

    public void inputMessage(String text){
        basicElements.contactUsMessage.val(text);
    }

    public void clickSendButton(){
        basicElements.sendButton.click();
    }

    public void messageSentContent(){
        basicElements.messageSent.shouldBe(Condition.visible);
    }
}

