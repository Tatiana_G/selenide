package cases;

import com.codeborne.selenide.Condition;
import locators.BasicElements;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;

public class MakeOrder {

    BasicElements basicElements = new BasicElements();

   /* private SelenideElement dressButton = $(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/a"));
    private SelenideElement casualDressesElement = $(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/ul/li[1]/a"));
    private SelenideElement firstDress = $(By.xpath("//*[@id=\"center_column\"]/ul/li/div/div[1]/div"));
    private SelenideElement addToCartButton = $(By.xpath("//*[@id=\"center_column\"]/ul/li/div/div[2]/div[2]/a[1]"));
    private SelenideElement successfullyAddedString = $(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[1]/h2/i"));
    private SelenideElement proceedToCheckoutButton = $(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a"));
    private SelenideElement cartPTCButton = $(By.xpath("//*[@id=\"center_column\"]/p[2]/a[1]/span"));
    private SelenideElement addressProceedToCheckoutButton = $(By.xpath("//*[@id=\"center_column\"]/form/p/button/span"));
    private SelenideElement shippingCheckbox = $(By.xpath("//*[@id=\"cgv\"]"));
    private SelenideElement shippingPTCButton = $(By.xpath("//*[@id=\"form\"]/p/button"));
    private SelenideElement payByBankWire = $(By.xpath("//*[@id=\"HOOK_PAYMENT\"]/div[1]/div/p/a"));
    private SelenideElement confirmButton = $(By.xpath("//*[@id=\"cart_navigation\"]/button"));
    private SelenideElement orderConfirmation = $(By.xpath("//*[@id=\"center_column\"]/h1"));

    */


    public void mainPageIsOpen(){
        $(By.xpath("//*[@id=\"editorial_block_center\"]/h1")).shouldBe(Condition.visible);
    }

    public void chooseMenu() {
        basicElements.dressButton.hover();
    }

    public void chooseSubmenu(){
        basicElements.casualDressesElement.shouldBe(Condition.visible, Duration.ofSeconds(10)).click();
    }

    public void setupSizeCheckbox(){
        $(By.xpath("//*[@id=\"layered_id_attribute_group_1\"]")).click();
    }

    public void chooseDress(){
        basicElements.firstDress.hover();
        basicElements.addToCartButton.shouldBe(Condition.visible, Duration.ofSeconds(10)).click();
        basicElements.successfullyAddedString.shouldBe(Condition.visible, Duration.ofSeconds(30));
    }

    public void redirectToCart(){
        basicElements.proceedToCheckoutButton.click();
        basicElements.cartPTCButton.click();
    }

    public void choseAddressPTCButton(){
        basicElements.addressProceedToCheckoutButton.click();
        basicElements.shippingCheckbox.click();
        basicElements.shippingPTCButton.click();
    }

    public void setPayByBankWire(){
        basicElements.payByBankWire.click();
        basicElements.confirmButton.click();
    }

    public void confirmation(){
        basicElements.orderConfirmation.shouldBe(Condition.visible);
    }
}
