package cases;

import com.codeborne.selenide.Condition;
import locators.BasicElements;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SignIn {

    BasicElements basicElements = new BasicElements();

    /* private SelenideElement singInButton = $(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a"));
    private SelenideElement loginInput = $(By.xpath("//*[@id=\"email\"]"));
    private SelenideElement passwordInput = $(By.xpath("//*[@id=\"passwd\"]"));
    private SelenideElement yourLogo = $(By.xpath("//*[@id=\"header_logo\"]/a/img"));

     */

    public void clickButton(String text) {
        basicElements.singInButton.click();
    }

    public void inputLogin(String text) {
        basicElements.loginInput.val(text);
    }

    public void inputPassword(String text) {
        basicElements.passwordInput.val(text);
    }

    public void contentIsVisible(String arg0) {
        $(By.xpath("//*[@id=\"center_column\"]/h1")).shouldBe(Condition.visible);
    }

    public void clickSubmitLoginButton(String text) {
        $(By.xpath("//*[@id=\"SubmitLogin\"]")).click();
    }

    public void yourLogoClick(){
        basicElements.yourLogo.click();
    }
}
