package cases;

import com.codeborne.selenide.Condition;
import locators.BasicElements;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;

public class GetProductDetails {

    BasicElements basicElements = new BasicElements();

    public void chooseProduct(){
        basicElements.blackShirt.hover();
        basicElements.blackShirtMoreBtn.click();
        basicElements.imagineBlock.shouldBe(Condition.visible);
    }

    public void getImage(){
        basicElements.imagineBlock.click();
        basicElements.imageFancyBox.shouldBe(Condition.visible, Duration.ofSeconds(10));
        basicElements.imageFancyBoxClose.click();
        basicElements.imagineBlock.shouldBe(Condition.visible);
    }

    public void getMSize(){
        $("#group_1").selectOptionByValue("2");
    }

    public void getQuantity(){
        basicElements.itemQuantityPlusOneBtn.click();
        basicElements.itemColorWhite.click();
    }

    public void addToWishList(){
        basicElements.addToWishListBtn.click();
        basicElements.successfullyAddedWishList.shouldBe(Condition.visible, Duration.ofSeconds(10));
        basicElements.successfullyAddedWishListClose.click();
    }
}
