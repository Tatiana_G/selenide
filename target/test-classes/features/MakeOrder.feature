Feature: Make an order
  User need to make an order dress

  #Background:
  # When Click "Sign In" button
  # And Input login and password
  # And Click "Sign in" submit button
  # Then Redirect to main page

  Scenario: Make an order
   Given Main page is open
    When Choose menu
    Then Setup size checkbox
    Then Choose dress and add to cart
    Then Choose address and shipping
    Then Payment
    Then Confirm order
